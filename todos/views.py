from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import ToDoListForm, ToDoItemForm

# Create your views here.

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "show_list": lists,
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todolist": todolist,   
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = ToDoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ToDoListForm()
    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)

def todo_list_update(request, id):
    update_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = ToDoListForm(request.POST, instance=update_list)
        if form.is_valid():
            saved = form.save()
            return redirect("todo_list_detail", id=saved.id)
    else:
        form = ToDoListForm(instance=update_list)
    
    context = {
        "update_list": update_list
    }
    return render(request, "todos/update.html", context)
def todo_list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = ToDoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    
    else:
        form = ToDoItemForm()

    context = {
        "form": form
    }
    return render(request, "todos/createitem.html", context)

def todo_item_update(request, id):
    update = TodoItem.objects.get(id=id)
    if request.method =="POST":
        form = ToDoItemForm(request.POST, instance=update)
        if form.is_valid():
            saved_form = form.save()
            return redirect("todo_list_detail", id=saved_form.list.id)
    else:
        form = ToDoItemForm(instance=update)
    context = {
        "form": form
    }
    return render(request, "todos/updateitem.html", context)