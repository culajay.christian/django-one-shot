from django import forms
from todos.models import TodoList, TodoItem
from django.forms import ModelForm

class ToDoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]

class ToDoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]